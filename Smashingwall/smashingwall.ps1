﻿# Downloads specified wallpapers from SmashingMagazines Montly wallpaper post
# Requires manual input, because you won't like all the wallpaper and don't need all of the resolutions

# Set wallpaper directory
$wd = 'C:\Users\Gebruiker\Pictures\wallpaper'
# Get current year
$AD = Get-Date -Format 'yyyy'
# Get current Month
$MM = Get-Date -Format 'MM'
#Test if folder of current year exists
$year = Test-Path $wd\$AD

. .\wallpaperfunctions.ps1

test-year
month-wall
input-links
get-walls
sort-wall
Clear-Host
Write-Host "Done"

