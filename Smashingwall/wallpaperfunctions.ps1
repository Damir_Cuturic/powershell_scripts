function test-year{
If ($year -eq $TRUE)
  {
  cd $wd\$AD
  }
Else
  {
  md $wd\$AD
  cd $wd\$AD
  }
}

function month-wall{
md $MM
cd $MM
md cal, no-cal
New-Item -ItemType File '.\url.txt'
}

function input-links{
Write-Host "Paste links in url{Space}url{Space}...url{Space} format"
$input = Read-Host
$input.Split(" ") | Out-File .\url.txt -Encoding utf8
}

function get-walls{
$gs = get-item .\url.txt
$sz = $gs.length
If ($sz -gt 0) {
wget.exe -i .\url.txt
}
Else{
  Write-Host "Error :: url.txt is Empty"
}
}

function sort-wall{
ls -Filter "*-cal-*" | mv -destination .\cal\
ls -Filter "*-nocal-*" | mv -destination .\no-cal\
}
