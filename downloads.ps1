# PowerShell Version : 4.0
$DLFolder = "$HOME\Downloads"
Push-Location
Set-Location $DLFolder
Write-Output "change folder $DLFolder"
$img = Test-Path $DLFolder\01_img
If ($img -eq $TRUE)
  {
  Get-ChildItem -Path .\* -Include *.jpg, *.png, *.gif | Move-Item -Destination .\01_img
  }
Else
    {
    New-Item -Path $DLFolder\01_img
    Get-ChildItem -Path .\* -Include *.jpg, *.png, *.gif | Move-Item -Destination .\01_img
    }
write-output "Images Moved"
$doc = Test-Path $DLFolder\02_doc
If ($doc -eq $TRUE)
  {
  Get-ChildItem -Path .\* -Include *.doc*, *.ppt*, *.xls*, *.pdf | Move-Item -Destination .\02_doc
  }
Else
  {
  New-Item -Path $DLFolder\02_doc
  Get-ChildItem -Path .\* -Include *.doc*, *.ppt*, *.xls*, *.pdf | Move-Item -Destination .\02_doc
  }
write-output "Docs Moved"
$zip = Test-Path $DLFolder\03_zip
If ($zip -eq $TRUE)
  {
  Get-ChildItem -Path .\* -Include *.zip, *.rar, *.7z, *.tar* | Move-Item -Destination .\03_zip
  }
Else
  {
  New-Item -Path $DLFolder\03_doc
  Get-ChildItem -Path .\* -Include *.zip, *.rar, *.7z, *.tar* | Move-Item -Destination .\03_zip
  }
write-output "Zips Moved"
Get-ChildItem -Path .\04_music -Include *.mp3 | Move-Item -Destination F:\Music\Sort
write-output "mp3 Moved"
Get-ChildItem -Path .\04_music -Include *.wav | Move-Item -Destination F:\Music\Sort\00_Convert
write-output "wav Moved"
Pop-Location
exit
