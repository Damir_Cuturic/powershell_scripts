﻿#Powershell Script to create a text file backup of the F:\ drive containing all the
#This will be used as a monitoring tool to see how the database is growing
#

$A = Get-Date -Format "dd-MM-yyyy_HHmm"
$B = Get-Date -Format "dd-MM-yyyy"

#Setting a variable so the exact date can be traced back when a file is removed
#Setting a variable with the general date 

cd F:\Music
ls -r > ..\log\"$A-list.txt"
#Moving to the F:\Music directory
#Create a file containing a list per directory 
 
md ..\log\$B 
ls -r -name -file > ..\log\$B\"songs-$A.txt"
#Creates a folder with the current date
#Creates a file in the currentdate folder with the specific time

cd I:\Music\Music
ls -r > ..\log\"$A-list.txt"
#Moving to the I:\Music directory
#Create a file containing a list per directory 

Write-Host "DONE"
Write-Host "see the scripts at F:\log\<DATE_TIME>.txt"
Write-Host "see the scripts at I:\Music\log\<DATE_TIME>.txt"