# Make project folder structure
# Author: Damir Cuturic
# Date  : 31-05-2016

# Vault location
$vault = 'C:\Vault\Projects'
# Input project number
$project = Read-Host 'Enter Project Number:'

# Add test for valid project number

# end test

# Make directory
New-Item -Path $vault -Name $project -ItemType directory
# change directory to $project
Set-Location $vault\$project
# Make subdirectories
md -Path CAD,DOC,PIC 

# Send Done Message
Write-Host "Done"

# Write test to see if folder needs to be opened 
# end test

# uncomment to open created directory instantly
# explorer $vault\$project


# Press enter to close window 
# Perhaps a bit too much 
Read-Host -Prompt �Press Enter to exit�