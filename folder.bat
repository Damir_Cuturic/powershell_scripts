echo off
echo "Enter Project name"
set /p project=Enter project name:

md "%project%"
cd "%project%"

md "00_Temp"
md "01_Documents"
md "02_External_Assets"
md "03_CAD"
md "04_Renders"
md "05_Templates"
echo "First Draft" > _Draft.txt
echo "To Do" >_TODO.txt
tree
pause
cd ..
