#Repo of my powershell scripts folder

For backup purposes

##Usage
Script in their own folder are considered good enough
###*Downloads.ps1*

Just run this one.
It will do the following:

1. move to your $home\downloads directory

2. It will move files ending on  \[ \*.jpg, \*.png, \*.gif \] to the 01\_img folder

	3. It will move files ending with \[ \*.doc\*, \*.ppt\*, \*.xls\*, \*.pdf \] to the 02\_doc folder

	4. It moves files ending with \[ \*.zip, \*.rar, \*.7z, \*.tar\* \] to the 03\_zips folder

5. it will move all \*.mp3 files to the F:\Music drive

6. Move all \*.wav files to 00\_Convert folder for converting

###*folder.bat*

_folder.bat_ creates a project folder structure

**in development**

###*music.ps1*
_music.ps1_ does several things.

1. It makes a file in F:\log\<date_time>-list.txt this is for backup purposes

2. It does the same thing for the G:\ drive

3. Makes a relative path log of all the files in F:\music under F:\log\<date>\songs\_<date\_time>.txt

###*SmashingwWall.ps1*
_SmashingWall.ps1_ makes getting wallpapers from Smashing Magazine a bit more streamlined process

1. Checks if directory exists, creates if not

2. Creates folder for the current month and the sub folders _cal_ and _no-cal_

3. Asks for urls separated with spaces 

4. Splits urls and saves each to file.txt 

5. Downloads the wallpapers and places them in the appropriate folder

wget.exe is a dependency see the wiki on how to change this to make use of native Powershell
