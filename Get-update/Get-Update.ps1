#Source: http://serverfault.com/questions/135191/shorcut-to-list-pending-updates-in-microsoft-windows

$update = new-object -com Microsoft.update.Session
$searcher = $update.CreateUpdateSearcher()
$pending = $searcher.Search("IsInstalled=0")
foreach($entry in $pending.Updates)
{
    Write-Output "Title: " $entry.Title
    Write-Output "Downloaded? " $entry.IsDownloaded
    Write-Output "Description: " $entry.Description
    foreach($category in $entry.Categories)
    {
        Write-Output "Category: " $category.Name
    }
    Write-Output " "
}